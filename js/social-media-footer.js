/******************************
/ AIESEC KO
/*****************************/

function AiesecViewModel(){

	self = this;

	/******************************
	/ Facebook
	/*****************************/

	self.facebookLikeCount = ko.observable();

	self.getFacebookLikes = function(){
		this.url = "https://api.facebook.com/method/fql.query?query=select%20like_count%20from%20link_stat%20where%20url=%27https://www.facebook.com/AIESECglobal%27&format=json";

		jQuery.getJSON(this.url).done(function(data){
			self.facebookLikeCount(data[0].like_count);
		})
	}

	/******************************
	/ Instagram
	/*****************************/

	self.instagramArray = ko.observableArray([]);

	self.instagramClientID = "dfac8765eae44be2b7a419c98a553661";

	self.instagramImage = function(obj){
		this.link = obj.link || "#";
		this.image = obj.images.low_resolution.url || "#";
	}

	self.url = window.location;

	self.instagramToken = "183254376.dfac876.ad1387e27abe433285a5490360cc22b0";

	self.instagramMediaCount = ko.observable();

	self.getInstagramFeed = function(){

		// Get info
		jQuery.ajax({
			url: "https://api.instagram.com/v1/tags/aiesec?access_token=" + self.instagramToken,
			dataType: 'jsonp',
			success: function(data){
				self.instagramMediaCount(data.data.media_count);
			}
		});

		// Get media
		jQuery.ajax({
			url: "https://api.instagram.com/v1/tags/aiesec/media/recent?count=28&access_token=" + self.instagramToken,
			dataType: 'jsonp',
			success: function(data){
				for(var i = 0; i < data.data.length; i++){
					self.instagramArray.push(new self.instagramImage(data.data[i]));
				}
			}
		});

	}

}

/******************************
/ Apply bindings
/*****************************/

aiesecViewModel = new AiesecViewModel();

ko.applyBindings(aiesecViewModel);

/******************************
/ Load social media data
/*****************************/

aiesecViewModel.getInstagramFeed();
aiesecViewModel.getFacebookLikes();