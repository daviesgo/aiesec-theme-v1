<!DOCTYPE html>
<html <?php language_attributes(); ?> class="breakpoint-wide-screen breakpoint-tablet breakpoint-mobile">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style-responsive.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/aiesec.css" />

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="doc" class="doc">
		<div id="wrapper" class="hfeed">
			<header id="header" role="banner">
				<div class="wrapper">
					<a href="/" id="branding">AIESEC</a>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<span id="mobile-menu-title">Menu</span>
					<div class="collapse navbar-collapse">
						<?php
							wp_nav_menu(
								array(
									'theme_location' 	=> 'main-menu',
									'container'			=> 'false'
								)
							);
						?>
					</div>
				</div>
			</header>
			<div id="container">