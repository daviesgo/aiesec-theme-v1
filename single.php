<?php get_header(); ?>
<section id="content" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php
			if(has_post_thumbnail()){
				$image =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0];
			}
		?>

		<section class="vp-section content" id="featured-news-header" style="background-image: url(<?php echo $image; ?>)">
			<div class="section-inner">
				<div class="wrapper">
					<div class="vp-grids">
						<div class="vp-grid-8 vp-offset-2" id="featured-news-column">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="vp-section content" id="news">
			<div class="section-inner">
				<div class="wrapper">
					<div class="vp-grids">
						<div class="vp-grid-8 vp-offset-2" id="news-column">
							<p><?php the_field('location'); ?></p>
							<p><?php the_field('original_post_date'); ?></p>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>


	<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>