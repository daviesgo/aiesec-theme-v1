<?php /* Template Name: News */ ?>
<?php get_header(); ?>
<section id="content" role="main">
	<section class="entry-content">
		<article>
			<?php

				// Get all posts

				$news_args = array(
					'posts_per_page'   => 4,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_type'        => 'post',
					'post_status'      => 'publish',
					'suppress_filters' => true
				);

				$news_posts = get_posts($news_args);

				$news_index = 0;

				foreach($news_posts as $post) : setup_postdata($post);

				if($news_index == 0):

					if(has_post_thumbnail($post->ID)) {
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "full")[0];
					}

			?>

				<section class="vp-section content" id="featured-news-header" style="background-image: url(<?php echo $image; ?>)">
					<div class="section-inner">
						<div class="wrapper">
							<div class="vp-grids">
								<div class="vp-grid-8 vp-offset-2" id="featured-news-column">
									<h1><?php the_title(); ?></h1>
									<p><?php the_field('location'); ?></p>
									<p><?php the_field('original_post_date'); ?></p>
									<div id="featured-news-content">
										<?php the_content(); ?>
									</div>
									<a class="vp-button vp-button-small vp-mode-default" id="featured-news-button" href="<?php the_permalink(); ?>">Read More</a>
								</div>
							</div>
						</div>
					</div>

				</section>


				<section class="vp-section content" id="news">
					<div class="section-inner">
						<div class="wrapper">
							<div class="vp-grids">
								<h2 id="news-title">AIESEC in the news</h2>


			<?php else: ?>

								<!-- NORMAL NEWS -->

								<?php

								if(has_post_thumbnail($post->ID)) {
									$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "full")[0];
								}

								?>

								<div class="vp-grid-4 news-column">

									<img src="<?php echo $image; ?>" />

									<p class="news-tile-title"><strong><?php the_title(); ?></strong></p>
									<p><?php the_field('location'); ?></p>
									<p><?php the_field('original_post_date'); ?></p>
									<?php the_content(); ?>
									<a class="vp-button vp-button-large vp-mode-default news-button" href="<?php the_permalink(); ?>">Read More</a>
								</div>

							</div>
						</div>
					</div>

			<?php

				endif;

			?>

			<?php
				$news_index++;
				endforeach;
				wp_reset_postdata();
			?>

			</section>

		</article>
	</section>
</section>
<?php get_footer(); ?>