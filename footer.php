			<div class="clear"></div>
		</div>
		<footer id="aiesec-social-media-footer" role="contentinfo">
			<div class="wrapper">
				<div class="vp-grids">
					<section id="social-media-right-column" class="vp-grid-4">
						<div class="social-media-module social-media-module-twitter">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter-logo.png" />
							<h6><span data-bind="text: facebookLikeCount"></span><p>followers</p></h6>
						</div>
						<div class="social-media-module social-media-module-linkedin">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin-logo.png" />
							<h6><span data-bind="text: facebookLikeCount"></span><p>connections</p></h6>
						</div>
						<div class="social-media-module social-media-module-facebook">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-logo.png" />
							<h6><span data-bind="text: facebookLikeCount"></span><p>connections</p></h6>
						</div>
					</section>
					<section id="social-media-right-column" class="social-media-module social-media-module-instagram vp-grid-8">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram-logo.png" />
						<h6><span data-bind="text: instagramMediaCount"></span><p>photos</p></h6>
						<!-- ko foreach: instagramArray -->
							<div class="instagram-feed-item">
								<a data-bind="attr: {href: link}" target="_blank">
									<img data-bind="attr: {src: image, class: 'instagram-image'}" />
								</a>
							</div>
						<!-- /ko -->
					</section>
				</div>
			</div>
		</footer>
		<footer id="aiesec-footer" role="contentinfo">
			<?php

				if(function_exists('footer_text')) {
					footer_text();
				}

			?>
		</footer>
	</div>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/knockout-3.2.0.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/html5shiv.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/respond.min.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.flexslider-min.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/get-style-property.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/get-size.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.isotope.min.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/mediaelement-and-player.min.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js?ver=3.8"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/social-media-footer.js?ver=3.8"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- <script type="text/javascript" src="http://platform.linkedin.com/in.js">
		api_key: "77qjdao46mk4bu";
	</script> -->


	<?php wp_footer(); ?>
</body>
</html>